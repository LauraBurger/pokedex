const VIGNETTE = `
<div class="col-3 mt-2">
    <div class="card">
        <div id="carouselExampleIndicators" class="carousel slide bg-dark" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                {{#if data.sprites.front_default}}
                <div class="carousel-item active">
                    <img class="d-block w-100" src="{{data.sprites.front_default}}" alt="Photo de face de {{monPokemon}}">
                </div>
                {{/if}}
                {{#if data.sprites.back_female}}
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{data.sprites.back_female}}" alt="Photo de dos de femmelle {{monPokemon}}">
                </div>
                {{/if}}
                {{#if data.sprites.back_shiny}}
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{data.sprites.back_shiny}}" alt="Photo brillante de dos de {{monPokemon}}">
                </div>
                {{/if}}
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="card-body">
            <h5 class="card-title">{{data.name}}</h5>
            <p class="card-text">
                {{#each data.types}}
                    <span class="badge badge-primary">{{this.type.name}}</span>
                {{/each}}
            </p>
        </div>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-{{monPokemon}}">
            Détails
        </button>
    </div>     
        <div class="modal fade" id="modal-{{monPokemon}}" tabindex="-1" aria-labelledby="modal-label-{{monPokemon}}" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-label-{{monPokemon}}">Fiche de {{monPokemon}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <ul class="list-group list-group-flush">
            {{#each data.stats}}
            <li class="list-group-item">
                <p>{{this.stat.name}}</p>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped" role="progressbar" style="width: {{this.base_stat}}%;" aria-valuenow="{{this.base_stat}}" aria-valuemin="0" aria-valuemax="100">
                        {{this.base_stat}}
                    </div>
                </div> 
            </li>    
            {{/each}}               
        </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
            </div>
        </div>
    </div>
</div>`;

async function getPokemon(pok){
    const reponse = await axios.get(`https://pokeapi.co/api/v2/pokemon/${pok}`) 
    return reponse;
}

window.addEventListener("DOMContentLoaded", (event) => {
    var boutonDeRecherche = document.body.querySelector('#rechercher');
    boutonDeRecherche.addEventListener("click", async function(){
        var monPokemon = document.body.querySelector('#pokemon').value.toLowerCase();
        var reponse = await getPokemon(monPokemon);
        var data = reponse.data;
    
        var template = Handlebars.compile(VIGNETTE);

        
        document.body.querySelector('#resultat').insertAdjacentHTML('afterbegin', template({
            data: data,
            monPokemon: monPokemon
        }));
    });
});

